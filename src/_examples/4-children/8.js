/* Core */
import { render } from 'react-dom';

render(
    <h1 children="Добро пожаловать в Лабораторию!" />,
    document.getElementById('root'),
);
