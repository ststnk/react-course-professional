/* Instruments */
import { icons } from '../theme/icons/tag';

export const getTagIcon = (tag: string = 'JavaScript') => {
    // @ts-expect-error
    const TagIcon = icons[tag];

    return TagIcon;
};
