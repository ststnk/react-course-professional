export const formatDate = (date: string | Date) => {
    return new Date(date).toLocaleDateString('ru-RU', {
        hour: '2-digit',
        minute: '2-digit',
    });
};
