const SETTINGS_OPEN = 'translateX(0)';
const SETTINGS_CLOSED = 'translateX(100%)';
const DOCUMENT = document.documentElement;
const IS_SETTINGS_OPEN_VAR = '--isSettingsOpen';

export const closeSettings = () => {
    DOCUMENT.style.setProperty(IS_SETTINGS_OPEN_VAR, SETTINGS_CLOSED);
};

export const openSettings = () => {
    DOCUMENT.style.setProperty(IS_SETTINGS_OPEN_VAR, SETTINGS_OPEN);
};

export const isSettingsOpen = () => {
    return (
        getComputedStyle(DOCUMENT).getPropertyValue(IS_SETTINGS_OPEN_VAR) ===
        SETTINGS_OPEN
    );
};
