import { Home } from './Home';
import { Publish } from './Publish';
import { Tag } from './Tag';
import { Search } from './Search';
import { Settings } from './Settings';

export const icons = {
    Home,
    Publish,
    Tag,
    Search,
    Settings,
};
