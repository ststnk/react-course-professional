/* Instruments */
import { TagEnum, TagClass } from './TagClass';
import { TipClass } from './TipClass';

export const tags = [
    new TagClass(TagEnum.JAVASCRIPT),
    new TagClass(TagEnum.TYPESCRIPT),
    new TagClass(TagEnum.REACT),
    new TagClass(TagEnum.VSCODE),
    new TagClass(TagEnum.NEXT_JS),
    new TagClass(TagEnum.NODE_JS),
    new TagClass(TagEnum.CSS),
    new TagClass(TagEnum.GRAPHQL),
    new TagClass(TagEnum.TESTING),
    new TagClass(TagEnum.NPM),
    new TagClass(TagEnum.GIT),
    new TagClass(TagEnum.TOOLS),
    new TagClass(TagEnum.MACOS),
];

export const tips = [
    new TipClass(
        TagEnum.JAVASCRIPT,
        'Пользуйся правильными переменными',
        'Не var а const и let',
    ),
    new TipClass(TagEnum.JAVASCRIPT, 'Помни о приведении типов', 'Не == а ==='),
    new TipClass(
        TagEnum.JAVASCRIPT,
        'Способы создания массива любой длины',
        'Array.from({ length: 5}); [ ...Array(5).keys() ]',
    ),
    new TipClass(
        TagEnum.JAVASCRIPT,
        'Знай все falsy-значения',
        "undefined, null, 0, false, NaN, '' — это falsy-значения!",
    ),
    new TipClass(
        TagEnum.JAVASCRIPT,
        'Умей фильтровать лишнее',
        "[false, false, 'hello', true, undefined].filter() — ['hello', true]",
    ),
    new TipClass(TagEnum.TYPESCRIPT, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.REACT, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.NEXT_JS, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.NODE_JS, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.CSS, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.UI_UX, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.GRAPHQL, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.VSCODE, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.NPM, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.TESTING, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.TOOLS, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.GIT, 'ЗАГОЛОВОК'),
    new TipClass(TagEnum.MACOS, 'ЗАГОЛОВОК'),
];
