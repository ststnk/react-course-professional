/* Core */
import { v4 } from 'uuid';
import { date, lorem } from 'faker';

/* Instruments */
import { TagEnum } from './TagClass';

export interface TipModel {
    id: string;
    title: string;
    preview: string;
    body: string;
    author: string;
    tag: string;
    created: string;
}

export class TipClass {
    id = v4();
    tag: TagEnum;
    title: string;
    author: string;
    body = lorem.sentences(40);
    created = date.recent();
    preview: string;

    constructor(
        tag: TagEnum,
        title: string,
        preview: string = 'Популярные библиотеки для React',
        author: string = 'Lectrum',
    ) {
        this.tag = tag;
        this.title = title;
        this.preview = preview;
        this.author = author;
    }
}
